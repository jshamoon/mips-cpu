# MIPS CPU

This VHDL script simulates a simple MIPS CPU architecture.

The CPU is capable of perfroming the following 32-bit MIPS instructions:
-Addi
-beq
-bne
-j
-lw
-sltiu
-sw
-addu
-sltu
-subu
-syscall

A ROM was also hardcoded with data which test the functionality of the CPU.
