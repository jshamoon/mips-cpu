library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CPU is
begin 
	port(
	-- physical inputs
	clk    :   IN std_logic;  
	reset  :   IN std_logic;

	-- physical outputs
	stop_bit  : OUT std_logic
	);
end entity CPU;

architecture a of CPU is
	
	-- creating our array for Instruction Memory (depth = 32 words)
	type t_Data is array (0 to 31) of std_logic_vector(31 downto 0);
	signal InstMem : t_Data;
	-- Storing our 26 instuctions within the array we just made
	InstMem[0]<="00100100000010100000000000000000";
	InstMem[1]<="00100100000010010000000000000001";
	InstMem[2]<="00100100000010000000000000000000";
	InstMem[3]<="00100100000010110000000000000100";
	InstMem[4]<="00100100000011000010000000000000";
	InstMem[5]<="10101101100010100000000000000000";
	InstMem[6]<="00000001010010010101000000100001";
	InstMem[7]<="00100101100011000000000000000100";
	InstMem[8]<="00101101010000010000000000010000";
	InstMem[9]<="00010100001000001111111111111011";
	InstMem[10]<="00100101100011000000000000001000";
	InstMem[11]<="00000001010010010101000000100011";
	InstMem[12]<="10101101100010101111111111111000";
	InstMem[13]<="00000001100010110110000000100001";
	InstMem[14]<="00010001010000000000000000000001";
	InstMem[15]<="00001000000000000000000000001011";
	InstMem[16]<="00100100000011000001111111111000";
	InstMem[17]<="00100100000010110000000000100000";
	InstMem[18]<="10001101100011010000000000001000";
	InstMem[19]<="00100101101011011000000000000000";
	InstMem[20]<="10101101100011010000000000001000";
	InstMem[21]<="00000001010010010101000000100001";
	InstMem[22]<="00100101100011000000000000000100";
	InstMem[23]<="00000001010010110000100000101011";
	InstMem[24]<="00010100001000001111111111111001";
	InstMem[25]<="00100100010000100000000000001010";
	InstMem[26]<="00000000000000000000000000001100";

	-- creating our array for data Memory (depth = 32 words)
	signal DataMem : t_Data;

    -- creating program counter register (initilize with value 0)
    signal PC : std_logic_vector(5 downto 0) :="00000";

    -- this will be used to store int value of PC
    signal PC_int : integer;

    -- creating reg file (depth = 32 words)
    signal regFile : t_Data;

    -- rs register number
    signal rs : std_logic_vector(5 downto 0);

    -- this will be used to store int value of rs
    signal rs_int : integer range 0 to 31;

    -- rt register number
    signal rt : std_logic_vector(5 downto 0);

    -- this will be used to store int value of rt
    signal rt_int : integer range 0 to 31;

    -- rd register number
    signal rd : std_logic_vector(5 downto 0);

    -- this will be used to store int value of rd
    signal rd_int : integer range 0 to 31;

 	-- this will hold immediate value (type I instructions)
    signal imm : std_logic_vector(15 downto 0);

    -- this will hold int value of imm
    signal imm_int : integer;

    -- this will hold signed-extended version of immediate value
    signal signExtImm : std_logic_vector(31 downto 0);

    -- this will hold address (type j instructions)
    signal address : std_logic_vector(25 downto 0);

    -- this will hold int int value of address
    signal address_int : integer;

    -- holds currrent instruction
    signal instruction : std_logic_vector(31 downto 0);

    -- holds opcode
    signal opcode : std_logic_vector(5 downto 0);

    -- holds function number
    signal funcNum : std_logic_vector(5 downto 0);

    -- constant used for extending 
    constant size : integer := 32;

    -- used to store memory address (int)  (dataMem[memAddr])
    signal memAddr : integer;

begin


run :process()
begin

PC_int <= to_integer(unsigned(PC));

instruction <= InstMem[PC_int];
opcode <= instruction(31 downto 26);

-- this case statment will act as our contorller
case( opcode ) is
 
    -- addiu
 	when "001001" =>
 		rs <= instruction(25 downto 21);
 		rs_int <= to_integer(unsigned(rs)); 
 		rt <= instruction(20 downto 16);
 		rt_int <= to_integer(unsigned(rt));
 		imm <= instruction(15 downto 0);
 		signExtImm <= std_logic_vector(resize(signed(imm), size));
 		regFile[rt_int] <= regFile[rs_int] + signExtImm;

 		PC <= PC + "00001"; 

 	-- beq
 	when "000100" =>
 		rs <= instruction(25 downto 21);
 		rs_int <= to_integer(unsigned(rs)); 
 		rt <= instruction(20 downto 16);
 		rt_int <= to_integer(unsigned(rt));
 		imm <= instruction(15 downto 0);
 		imm_int <= to_integer(signed(imm));

 		PC_int <= to_integer(unsigned(PC));

 		-- in theory i could just compart rs and rt... no need to convert then to ints
 		if rs_int = rt_int then
 			-- this relative addressing is all done in decimal
 			PC_int <= PC_int + 1 + imm_int;
 			PC <= std_logic_vector(to_unsigned(PC_int, 5)); -- I think we sould be able to use the int (5) directly here
 		else
 			PC <= PC + "00001"; 
 		end

 	-- bne
 	when "000101" =>
		rs <= instruction(25 downto 21);
 		rs_int <= to_integer(unsigned(rs)); 
 		rt <= instruction(20 downto 16);
 		rt_int <= to_integer(unsigned(rt));
 		imm <= instruction(15 downto 0);
 		imm_int <= to_integer(signed(imm));

 		PC_int <= to_integer(unsigned(PC));

 		-- in theory I could just compart rs and rt... no need to convert then to ints
 		if rs_int /= rt_int then
 			-- this relative addressing is all done in decimal
 			PC_int <= PC_int + 1 + imm_int;
 			PC <= std_logic_vector(to_unsigned(PC_int, 5)); -- I think we sould be able to use the int (5) directly here
 		else
 			PC <= PC + "00001"; 
 		end


 	-- j
 	when "000010" =>
 			address <= Instruction(25 downto 0);
 			address_int <= to_integer(unsigned(address));

 			PC_int <= address_int;
			PC <= std_logic_vector(to_unsigned(PC_int, 5));

 	-- lw
 	when "100011" =>
		rs <= instruction(25 downto 21);
 		rs_int <= to_integer(unsigned(rs)); 
 		rt <= instruction(20 downto 16);
 		rt_int <= to_integer(unsigned(rt));
 		imm <= instruction(15 downto 0);
 		signExtImm <= std_logic_vector(resize(signed(imm), size));
 		imm_int <= to_integerto_integer(signed(signExtimm));

 		memAddr <= ((to_integer((signed(regFile[rs_int])) + imm_int) - 8192)/4;

 		regFile[rt_int] <= dataMem[memAddr];

 		PC <= PC + "00001"; 

 	-- sltiu
 	when "001011" =>
 		rs <= instruction(25 downto 21);
 		rs_int <= to_integer(unsigned(rs)); 
 		rt <= instruction(20 downto 16);
 		rt_int <= to_integer(unsigned(rt));
 		imm <= instruction(15 downto 0);
 		signExtImm <= std_logic_vector(resize(signed(imm), size));

 		if regFile[rs_int] < signExtImm then 
 			regFile[rt_int] <= "00000000000000000000000000000001";
 		else
 			regFile[rt_int] <= "00000000000000000000000000000000";
 		end

 		PC <= PC + "00001"; 
 			
 	-- sw
 	when "101011" =>
 		rs <= instruction(25 downto 21);
 		rs_int <= to_integer(unsigned(rs)); 
 		rt <= instruction(20 downto 16);
 		rt_int <= to_integer(unsigned(rt));
 		imm <= instruction(15 downto 0);
 		signExtImm <= std_logic_vector(resize(signed(imm), size));
 		imm_int <= to_integerto_integer(signed(signExtimm));

 		memAddr <= ((to_integer((signed(regFile[rs_int])) + imm_int) - 8192)/4;

 		dataMem[memAddr] <= regFile[rt_int]

 		PC <= PC + "00001"; 


 	-- addu,sltu,subu syscall
 	-- type r instrucntion all have 0 as opcode but have different function numbers
 	when "000000" =>
 		funcNum <= instruction(5 downto 0);
 		case(funcNum) is

 		-- addu
 		when "100001" =>
	 		rs <= instruction(25 downto 21);
	 		rs_int <= to_integer(unsigned(rs)); 
	 		rt <= instruction(20 downto 16);
	 		rt_int <= to_integer(unsigned(rt));
	 		rd <= instruction(15 downto 11);
	 		rd_int <= to_integer(unsigned(rd));

	 		dataMem[rd_int] <= dataMem[rs_int] + dataMem[rt_int];

	 		PC <= PC + "00001"; 

 		-- sltu
 		when "101011" =>

 			rs <= instruction(25 downto 21);
	 		rs_int <= to_integer(unsigned(rs)); 
	 		rt <= instruction(20 downto 16);
	 		rt_int <= to_integer(unsigned(rt));
	 		rd <= instruction(15 downto 11);
	 		rd_int <= to_integer(unsigned(rd));

	 		if dataMem[rs_int] < dataMem[rt_int] then
	 			datamem[rt_int] <= "00000000000000000000000000000001";
	 		else
	 			datamem[rt_int] <= "00000000000000000000000000000000";
	 		end

	 		PC <= PC + "00001"; 

 		-- subu
 		when "100011" =>

 			rs <= instruction(25 downto 21);
	 		rs_int <= to_integer(unsigned(rs)); 
	 		rt <= instruction(20 downto 16);
	 		rt_int <= to_integer(unsigned(rt));
	 		rd <= instruction(15 downto 11);
	 		rd_int <= to_integer(unsigned(rd));

	 		dataMem[rd_int] <= dataMem[rs_int] - dataMem[rt_int];

	 		PC <= PC + "00001"; 

 		-- syscall
 		when "001100" =>
 			-- not sure what to do here tbh 
 			-- this is just a guess for now
 			stop_bit <= '1';

 		end case;

 
 end case ; 



end process;

end architecture a;
